#!/usr/bin/env python3

from html.parser import HTMLParser
from sys import argv

class Analyseur(HTMLParser):
    def __init__(self):
        super(Analyseur, self).__init__()
        self.lire = 0

    def handle_starttag(self, balise, attributs):
        if self.lire > 0:
            self.lire += 1
        elif balise == 'p':
            for attribut in attributs:
                if attribut[0] == 'class' and attribut[1] == 'text-article':
                    self.lire += 1

    def handle_endtag(self, balise):
        if self.lire > 0:
            self.lire -= 1
        else:
            self.lire = 0

    def handle_data(self, données):
        if self.lire > 0:
            print(données)

if len(argv) < 2:
    print('Utilisation :', argv[0], '<fichier>')
    exit()

with open(argv[1], 'r') as fichier:
    tableau_lignes = fichier.readlines()
lignes = ""
for ligne in tableau_lignes:
    lignes += ligne

analyseur = Analyseur();
analyseur.feed(lignes)
